<?php

namespace Phops;

class Retry {

  /**
   * Retry execution multiple times.
   *
   * Example usage:
   *
   *   \Phops\Retry::retry(3, $callback); // With delay
   *   \Phops\Retry::retry(3, 0.5, $callback); // Without delay
   *
   */
  static function retry ($count, /** $delay = 0, */ $callback) {
    $delay = 0;
    if (count(func_get_args()) == 3) {
      $delay = func_get_args()[1];
      $callback = func_get_args()[2];
    }
    $try = 1;
    while (true)
      try {
        return call_user_func($callback);
      } catch (\Exception $exception) {
        if ($try >= $count)
          throw $exception;
        if (is_callable($delay))
          usleep(call_user_func($delay, $try) * 1000000);
        else
          usleep($delay * 1000000);
        $try += 1;
      }
  }

}
